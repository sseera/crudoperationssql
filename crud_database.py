import mysql.connector
conn = mysql.connector.connect(
  host = "localhost",
  user = "root",
  password = "siddhu",
  database = "josh"
)

jupyter = conn.jupyter()

print("Welcome!")
print("0 - For creating a table")
print("1 - For creating a record")
print("2 - For reading the database")
print("3 - For reading tables in that database")
print("4 - For reading a specific table")
print("5 - For read all the records ")
print("6 - For read records specifically")
print("7 - For renaming a table")
print("8 - For adding a column")
print("9 - For updating a record")
print("10 - For deleting a database")
print("11 - For deleting a table")
print("12 - For deleting a record")
print("Enter the appropriate user_input for your desired operations")

user_input = int(input())

#Creating table
def create_table():
  jupyter.execute("CREATE TABLE mercedes (serial_no int, lastname VARCHAR(255))")
  print("Table Created!")
  pass
#Creating record
def create_record():

  sql_statement="INSERT INTO mercedes (serial_no, lastname) VALUES (%s, %s)"

  while True:
    value=(int(input("Enter id ")),input("Enter last name "))

    x = input("Do want to continue?, Enter y")

    if x != ( "y" or "yes"):
      break
  
  jupyter.execute(sql_statement,value)
  conn.commit()

  print(jupyter.rowcount, "record inserted.")
  print("inserted data succesfully")
#reading database
def read_database():
  sql_statement="SHOW DATABASES"
  jupyter.execute(sql_statement)

  for x in jupyter:
    print(x)
#reading all table
def read_tables():
  sql_statement="SHOW TABLES"
  jupyter.execute(sql_statement)
  for y in jupyter:
    print(y)
#reading_specific_table
def read_specific_table():

  sql_statement="DESCRIBE mercedes"
  jupyter.execute(sql_statement)

  for y in jupyter:
    print(y)

def read_all_records():
  sql_statement="SELECT * FROM mercedes"

  jupyter.execute(sql_statement)
  for j in jupyter:
    print(j)

def read_records_specific():
  sql_statement="SELECT lastname FROM mercedes WHERE serial_no=1"

  jupyter.execute(sql_statement)
  for k in jupyter:
    print(k)

def rename_table():
  sql_statement="ALTER table mercedes RENAME To mercedes_benz"

  jupyter.execute(sql_statement)
  print('Table name changed')

def add_column():
  sql_statement="ALTER table mercedes_benz ADD email varchar(255)"

  jupyter.execute(sql_statement)
  print("Column added succesfully")

def update_record():
  sql_statement="UPDATE mercedes_benz set lastname='SUV' WHERE serial_no=1"

  jupyter.execute(sql_statement)
  print("Record updated succesfully")

def delete_database():
  sql_statement="DROP DATABASE josh"

  print("WARNING!! : THE DATABASE WILLL BE PERMANENTLY DELETED")

  x = input("Do want to continue?, Enter y")

  if x != ( "y" or "yes"):
    jupyter.execute(sql_statement)

  print('Database deleted succesfully')

def delete_table():
  sql_statement="DROP TABLE mercedes_benz"
  
  print("WARNING!! : THE TABLE WILLL BE PERMANENTLY DELETED")

  x = input("Do want to continue?, Enter y")

  if x != ( "y" or "yes"):
    jupyter.execute(sql_statement)

    print('Table deleted succesfully')

def delete_record():
  sql_statement="DELETE from mercedes_benz where serial_no=1 and lastname='SUV'"

  jupyter.execute(sql_statement)

  print("Record is deleted succesfully")


if user_input==0:
  create_table()
if user_input==1:
  create_record()
if user_input==2:
  read_database()
if user_input==3:
  read_tables()
if user_input==4:
  read_specific_table()
if user_input==5:
  read_all_records()
if user_input==6:
  read_records_specific()
if user_input==7:
  rename_table()
if user_input==8:
  add_column()
if user_input==9:
  update_record()
if user_input==10:
  delete_database()
if user_input==11:
  delete_table()
if user_input==12:
  delete_record()


print("Done!")